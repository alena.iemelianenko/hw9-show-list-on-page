//1.Опишіть, як можна створити новий HTML тег на сторінці.
//можна використовувати document.createElement(tag) для створення нового елемента з тегом, після цього необхідно вставити цей елемент на сторінку. для цього є спеціалтний метод append. 

//2.Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Функція має такий синтаксис: elem.insertAdjacentHTML(where, html), де перший параметр - це спеціальне слово, що вказує, куди по відношенню до elem робити вставку. Значення має бути одним із наступних:
// 1.`beforebegin' - вставити html безпосередньо (перед відкриваючим тегом elem),
// 2.`afterbegin' - вставити html на початок elem (після відкриваючого тега elem),
// 3.`beforeend' - вставити html в кінець elem (перед закриваючим тегом elem),
// 4.`afterend' - вставити html безпосередньо після elem (після закриваючого тега elem).

//3.Як можна видалити елемент зі сторінки?
//метод Element.remove() видаляє елемент із DOM-дерева. 


//Завдання: 
let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let arr2 = ["1", "2", "3", "sea", "user", 23];

function arrToList (arr, elemDOM = document.body) {
  let list = document.createElement("ul");
  arr.forEach(element => {
    list.insertAdjacentHTML('beforeend',`<li>${element}</li>`);
  });
  return elemDOM.appendChild(list);
};

arrToList(arr2);
